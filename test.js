import HTTP from 'http'

const PORT = process.argv[2]
let req = "/querty"

HTTP.get(`http://localhost:${PORT}${req}`, (resp) => {
  let body = '';
 
  // A chunk of data has been recieved.
  resp.on('data', (chunk) => {
    body += chunk;
  });
 
  // The whole response has been received. Print out the result.
  resp.on('end', () => {
    console.log(body);
    if (body !== req) {
      console.log("TEST - FAIL")
      process.exit(123)
    } else {
      console.log("TEST - OK")
    }
  });
 
}).on("error", (err) => {
  console.log("Error: " + err.message);
});